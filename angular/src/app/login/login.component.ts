import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { AuthService } from '../shared/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    @ViewChild('f',{ static: false }) loginForm: NgForm;

    constructor(
      public router: Router,
      private authService: AuthService,
      private spinner: NgxSpinnerService
    ) {}

    ngOnInit() {
        if (this.authService.isAuthenticated()) {
            this.router.navigate(['/dashboard']);
        }
    }

    onLoggedin() {
        this.spinner.show();
        this.authService.signinUser(this.loginForm.value.phone,this.loginForm.value.password).subscribe(
            res=>{
                this.spinner.hide();
                if(res.success){
                    this.router.navigate(['/dashboard']);
                }
                else
                {
                    alert("Wrong username and password");
                    //this.toastrService.typeWarning(res.blogger);
                }
            },
            (error)=>{
                alert("Wrong username and password");
                this.spinner.hide();
            //    this.toastrService.typeWarning("Something Went Wrong");
            }
        );
    //    localStorage.setItem('isLoggedin', 'true');
    }
}
