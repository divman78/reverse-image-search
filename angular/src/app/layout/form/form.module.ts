import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { PageHeaderModule } from './../../shared';
import { FormsModule }   from '@angular/forms';
import { NgxSpinnerModule  } from 'ngx-spinner';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
    MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,MatSidenavModule,
    MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatAutocompleteModule } from '@angular/material';

    import {  ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, FormRoutingModule, PageHeaderModule, FormsModule,
        MatSelectModule,
        NgxSpinnerModule,
        ReactiveFormsModule,
        MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
        MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
        MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,
        MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule,MatSidenavModule,
        MatAutocompleteModule,
    ],
    declarations: [FormComponent]
})
export class FormModule {}
