import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SearchService } from '../../services/search.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    products : any;
    errMess: string;

    getWishlist(){
        this.spinner.show();
        this.searchService.getRecommdationList()
        .subscribe(product => { 
            this.spinner.hide();
            this.products = product.products;
            console.log(this.products);
        },
        errmess => {
            this.spinner.hide();
            this.errMess = <any>errmess
        });
    }

    constructor(private searchService: SearchService,private spinner: NgxSpinnerService) {
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'Reverse Image Search',
                text:
                    ''
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'All Ecommerce Websites under One Umbrella',
                text: ''
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Search Your Favourite Garments Here',
                text:
                    ''
            }
        );

        this.alerts.push(
            {
                id: 1,
                type: 'success',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            },
            {
                id: 2,
                type: 'warning',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            }
        );
    }

    ngOnInit() {
        this.getWishlist();
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
