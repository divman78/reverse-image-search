import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ProcessHttpmsgServiceService } from './process-httpmsg-service.service'
import { map,catchError  } from 'rxjs/operators';
import { mongoURL } from '../shared/baseurl'

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor( private http : HttpClient , private processHttpmsgService : ProcessHttpmsgServiceService ) { }

  getImageDetails(input: String): Observable<any>{
    return this.http.post<any>(mongoURL + 'getImages', 
      {"imgTag": input})
      .pipe(map(res => {
        return {'products': res.product};
      }))
      .pipe(catchError(error => { return this.processHttpmsgService.handleError(error); }))
  }

  getWishlist(){
    return this.http.get<any>(mongoURL + 'user/showWishlist')
      .pipe(map(res => {
        return {'products': res.product};
      }))
      .pipe(catchError(error => { return this.processHttpmsgService.handleError(error); }))
  }

  addToWishlist(product : any){
    return this.http.post<any>(mongoURL + 'user/addToWishlist', 
      {
        "image_host": product.image_host,
        "image_link" : product.image_link,
        "image_description" : product.image_description,
        "image_source" : product.image_source 
      })
      .pipe(map(res => {
        return {'status': res.success};
      }))
      .pipe(catchError(error => { return this.processHttpmsgService.handleError(error); }))
  }

  getRecommdationList(){
    return this.http.get<any>(mongoURL + 'user/showRecommadationlist')
      .pipe(map(res => {
        return {'products': res.product};
      }))
      .pipe(catchError(error => { return this.processHttpmsgService.handleError(error); }))
  }

  addToRecommdationList(product : any){
    return this.http.post<any>(mongoURL + 'user/addToRecommadationlist', 
      {
        "image_host": product.image_host,
        "image_link" : product.image_link,
        "image_description" : product.image_description,
        "image_source" : product.image_source 
      })
      .pipe(map(res => {
        return {'status': res.success};
      }))
      .pipe(catchError(error => { return this.processHttpmsgService.handleError(error); }))
  }

}
