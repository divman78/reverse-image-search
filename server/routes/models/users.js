const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    phone : 
    {
        type : String,
        unique : true,
        required : true
    },
    password : 
    {
        type: String,
        required: true,
    },
    Cart : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }],
    product : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }],
    email : 
    {
        type : String,
        unique : true,
        sparse : true
    },
},
{ usePushEach: true });

var Users = mongoose.model('User', userSchema);
module.exports = Users;