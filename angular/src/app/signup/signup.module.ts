import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { FormsModule }   from '@angular/forms';
import { NgxSpinnerModule  } from 'ngx-spinner';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SignupRoutingModule,
    FormsModule,
    NgxSpinnerModule,
  ],
  declarations: [SignupComponent]
})
export class SignupModule { }
