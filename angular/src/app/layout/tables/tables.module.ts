import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TablesRoutingModule } from './tables-routing.module';
import { TablesComponent } from './tables.component';
import { PageHeaderModule } from './../../shared';
import { NgxSpinnerModule  } from 'ngx-spinner';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
    MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,MatSidenavModule,
    MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatAutocompleteModule } from '@angular/material';


@NgModule({
    imports: [CommonModule, TablesRoutingModule, PageHeaderModule,
        MatSelectModule,
        NgxSpinnerModule,
        MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
        MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
        MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,
        MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule,MatSidenavModule,
        MatAutocompleteModule,
    ],
    declarations: [TablesComponent]
})
export class TablesModule {}
