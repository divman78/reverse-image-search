import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Http, Response, HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ProcessHttpmsgServiceService } from '../../services/process-httpmsg-service.service';
import { Observable } from 'rxjs';
import { map,catchError  } from 'rxjs/operators';
import { mongoURL } from '../baseurl'



class AuthLoginResponse{
  token: string;
  status: any;
  blogger: any;
}

interface JWTResponse {
  status: string,
  success: string,
  user: any
};

@Injectable()
export class AuthService {
  token: string;
  authToken: string;

  tokenKey: string = 'JWT';
  name: Subject<string> = new Subject<string>();
  fname : string;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgServiceService ) {}

  checkJWTtoken() {
    this.http.get<JWTResponse>(mongoURL + 'user/checkJWTtoken')
    .subscribe(res => {
      //console.log("JWT Token Valid: ", res);
      this.sendUsername(res.user.username);
    },
    err => {
      //console.log("JWT Token invalid: ", err);
      this.destroyUserCredentials();
    })
  }

  sendUsername(name: string) 
  {
    this.name.next(name);
  }

  clearUsername() {
    this.name.next(undefined);
  }

  loadUserCredentials() {
    var credentials = JSON.parse(localStorage.getItem(this.tokenKey));
    if (credentials && credentials.name != undefined) {
      this.useCredentials(credentials);
    }
  }

  storeUserCredentials(credentials: any) {   
    localStorage.setItem(this.tokenKey, JSON.stringify(credentials));
    this.useCredentials(credentials);
  }

  useCredentials(credentials: any) {
    this.sendUsername(credentials.name);
    //console.log("usecredentials*******");
    //console.log(credentials.token);
    this.authToken = credentials.token;
    this.token = credentials.token;
    this.fname = credentials.name;
  }

  destroyUserCredentials() {
    this.authToken = undefined;
    this.clearUsername();
    localStorage.removeItem(this.tokenKey);
  }
  signupUser(phone:string, password: string, name : string, email: string): Observable<any>{
    //your code for signing up the new user
    //console.log(blogger);
    return this.http.post<any>(mongoURL + 'user/register',
    {
      "phone" : phone,
      "password" :password,
      "email" : email,
      "name" : name,
    }).pipe(map(res => {;
      if(res["success"])
      return {"success" : res["success"] };
    }))
    .pipe(catchError(error => { 
      //console.log(error);
      return this.processHTTPMsgService.handleError(error); 
    }));
  }
  

  signinUser(phone: string, password: string): Observable<any> {
    //your code for checking credentials and getting tokens for for signing in user
    //console.log(loginForm);
    return this.http.post<AuthLoginResponse>(mongoURL + 'user/login',
    {
      "phone": phone,
      "password": password
    }).pipe(map((res) => {
      //console.log("hello");
      if(res["success"])this.storeUserCredentials({name: res.status, token: res.token});
      return {"success" : res["success"], "token":res.token };
    }))
    .pipe(catchError(error => { 
      //console.log(error);
      return this.processHTTPMsgService.handleError(error); 
    }));
  }

  logout() {   
    this.token = null;
    this.authToken = null;
    this.destroyUserCredentials();
    this.router.navigate(['login']);

  }

  getToken() { 
    //console.log("get token:::");
    //console.log(this.authToken);   
    return this.authToken;
  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token 
    if(this.authToken){
      return true;
    }
    else{
      return false;
    }
    //return true;
  }

  getName() : string{
    return this.fname;
  }
}
