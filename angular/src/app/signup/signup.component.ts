import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { AuthService } from '../shared/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})

export class SignupComponent implements OnInit {

    @ViewChild('f',{ static: false }) registerForm: NgForm;

    constructor(
      public router: Router,
      private authService: AuthService,
      private spinner: NgxSpinnerService
    ) {}

    ngOnInit() {
        if (this.authService.isAuthenticated()) {
            console.log("valid");
            this.router.navigate(['/dashboard']);
        }
    }

    onRegister() {
        this.spinner.show();
        this.authService.signupUser(this.registerForm.value.phone,this.registerForm.value.password, this.registerForm.value.name, this.registerForm.value.email).subscribe(
            res=>{
                this.spinner.hide();
                if(res.success){
                    alert("Successfully Registered");
                    this.router.navigate(['/login']);
                }
                else
                {
                    alert("Something Went Wrong");
                    //this.toastrService.typeWarning(res.blogger);
                }
            },
            (error)=>{
                alert("Wrong username and password");
                this.spinner.hide();
            //    this.toastrService.typeWarning("Something Went Wrong");
            }
        );
    //    localStorage.setItem('isLoggedin', 'true');
    }
}