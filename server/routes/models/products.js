const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    image_host: {
        type: String,
        required: true
    },
    image_source: {
        type: String,
        required: true
    },
    image_link:{
        type : String,
        required : true
    },
    image_description:{
        type: String,
        required: true
    }
});

var Products = mongoose.model('Product', ProductSchema);

module.exports = Products;