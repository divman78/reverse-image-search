import tensorflow as tf
import matplotlib.pyplot as plt
import cv2
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
import pickle
import webcolors
import sys
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

path = sys.argv[1]



num_to_items={0:"T-shirt/top",1:"Trouser",2:"Pullover",3:"Dress",4:"Coat",5:"Sandal",6:"Shirt",7:"Sneaker",8:"Bag",9:"Ankle boot"}

pickle_in = open("fashion_model.pickle","rb")
model = pickle.load(pickle_in)

def makebgdark(fashion_img):
    mid_index = (int(fashion_img.shape[0]/2), int(fashion_img.shape[1]/2))
    bg_value = (int(fashion_img[0][0][0]) + int(fashion_img[0][0][1]) + int(fashion_img[0][0][2]))/3;
    obj_value = (int(fashion_img[mid_index[0]][mid_index[1]][0]) + int(fashion_img[mid_index[0]][mid_index[1]][1]) + int(fashion_img[mid_index[0]][mid_index[1]][2]))/3;
    if(bg_value>obj_value):
        return 255-fashion_img
    else:
        return fashion_img

def predictFashionItem(fashion_img):
    fashion_img = makebgdark(fashion_img)
    fashion_img = cv2.cvtColor(fashion_img, cv2.COLOR_BGR2GRAY)
    fashion_img = cv2.resize(fashion_img, (28,28))
    #plt.imshow(fashion_img)
    fashion_img = fashion_img.reshape(1,28,28,1)
    fashion_img = fashion_img/255.0
    
    
    predict_array = model.predict(fashion_img)

    mx_chance = max(predict_array[0])
    hash = -1
    for i in range(0, len(predict_array[0])):
        if(mx_chance == predict_array[0][i]):
            hash = i
            
    return num_to_items[hash]

def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

def predictColor(fashion_img):
    mid_index = (int(fashion_img.shape[0]/2), int(fashion_img.shape[1]/2))
    obj_color = (fashion_img[mid_index[0]][mid_index[1]][0],fashion_img[mid_index[0]][mid_index[1]][1], fashion_img[mid_index[0]][mid_index[1]][2])
    #print(obj_color)
    return get_colour_name(obj_color)

def getTags(fashion_img):
    search_string = predictColor(fashion_img)[1] + " colour " + predictFashionItem(fashion_img)
    return search_string


def query(img_path):
	img = plt.imread(img_path)
	query_result = getTags(img)
	print(query_result)

query('./uploads/'+path)

