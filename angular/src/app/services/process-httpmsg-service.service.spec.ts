import { TestBed } from '@angular/core/testing';

import { ProcessHttpmsgServiceService } from './process-httpmsg-service.service';

describe('ProcessHttpmsgServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProcessHttpmsgServiceService = TestBed.get(ProcessHttpmsgServiceService);
    expect(service).toBeTruthy();
  });
});
