from google_images_download import google_images_download
import sys
imgTag = sys.argv[1]

response = google_images_download.googleimagesdownload()

arguments = {"keywords":imgTag,"limit":5, "metadata" : True}   #creating list of arguments
paths = response.download(arguments)   #passing the arguments to the function
print(paths)
