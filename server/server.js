var http = require("http");
var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors')
const ps = require('python-shell')
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var authenticate = require('./authenticate');
var multer  =   require('multer');
//var upload = multer({ dest: 'uploads/' })

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + ".jpg");
  }
});

var upload = multer({ storage : storage});


var port = process.env.PORT||8080;

var app = express();

app.use(express.static('public'))


var users = require('./routes/user');

app.use(logger('dev'));
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/user',users);

mongoose.connect('mongodb://127.0.0.1/db' , function(err,db)
{
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Connected Successfully to mongodb");
    }
});

app.listen(port,function(err)
{
    if(err)
    {
        throw err;
    }
    console.log('app is running at ' + port);
})

app.post('/profile', upload.single('avatar'), function (req, res, next) {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were an
    console.log(req.file.filename);
    var options = {
        args:[ req.file.filename ]
    }
    ps.PythonShell.run('./divakar.py',options, function (err, data) {
        if (err) res.send(err);
        res.json({
            "status" : true,
            "imgTag" : data
        })
    })
})

app.post('/getImages', authenticate.verifyUser, function(req,res){
    var options = {
        args:[ req.body.imgTag ]
    }
    ps.PythonShell.run('./script.py',options, function (err, data) {
        if (err) res.send(err);
        var s = [];
        for(var j=5;j<=17;j+=3){
            var temp = "";
            var flag = 0;
            for(var i=0;i<data[j].length;i++){
                if(flag == 1){
                    if(data[j][i] != '\'')
                    temp += data[j][i];
                    else{
                        if((data[j][i+1] >= 'a' && data[j][i+1] <= 'z') && (data[j][i-1] >= 'a' && data[j][i-1] <= 'z'))
                        temp += data[j][i];
                        else
                        temp += '"';
                    }
                    if(data[j][i] == "}"){
                        flag = 0;
                    }
                }
                else{
                    if(data[j][i] == '{'){
                        temp += '{';
                        flag = 1;
                    }
                }
            }
            var ret = JSON.parse(temp);
            s.push(ret);
        }
        res.json({
            "product" : s
        });
    });
})

process.on('uncaughtException',function(err)
{
    console.log('Caught Exception :'+err);
})