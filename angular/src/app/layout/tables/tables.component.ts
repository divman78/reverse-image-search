import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SearchService } from '../../services/search.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
    animations: [routerTransition()]
})
export class TablesComponent implements OnInit {
    constructor(private searchService: SearchService,private spinner: NgxSpinnerService) {}

    products : any;
    errMess: string;

    getWishlist(){
        this.spinner.show();
        this.searchService.getWishlist()
        .subscribe(product => { 
            this.spinner.hide();
            this.products = product.products;
            console.log(this.products);
        },
        errmess => {
            this.spinner.hide();
            this.errMess = <any>errmess
        });
    }

    ngOnInit() {
        this.getWishlist();
    }
}
