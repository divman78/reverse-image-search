import { Component,ViewChild, OnInit,ElementRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SearchService } from '../../services/search.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { mongoURL } from '../../shared/baseurl'
import { ProcessHttpmsgServiceService } from '../../services/process-httpmsg-service.service';
import { map,catchError  } from 'rxjs/operators';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})
export class FormComponent implements OnInit {

    @ViewChild('fileInput',{ static: false }) fileInput: ElementRef;

    products : any;
    errMess: string;

    searchForm: FormGroup;
    loading: boolean = false;
    selectedFile: File
    imgTag : any;

    createForm() {
        this.searchForm = this.fb.group({
          avatar: null
        });
    }

    constructor(private searchService: SearchService,private spinner: NgxSpinnerService,
        private processHttpmsgService : ProcessHttpmsgServiceService,
        private http: HttpClient,
        private fb: FormBuilder) {
        this.createForm();
    }

    ngOnInit() {}

    add(product : any){
        this.searchService.addToRecommdationList(product)
        .subscribe(product => { 
            // Nothing
        },
        errmess => {
            this.spinner.hide();
            this.errMess = <any>errmess
        });
    }

    getImages(input : string){
        console.log("Here");
        console.log(input);
        this.spinner.show();
        this.searchService.getImageDetails(input)
        .subscribe(product => { 
            this.spinner.hide();
            this.products = product.products;
            this.add(this.products[0]);
            this.add(this.products[1]);
        },
        errmess => {
            this.spinner.hide();
            this.errMess = <any>errmess
        });
    }

    onSubmit() {
        this.spinner.show();
        const uploadData = new FormData();
        uploadData.append('avatar', this.selectedFile, this.selectedFile.name);
        this.http.post(mongoURL + 'profile',uploadData)
        .subscribe(res => {
            this.spinner.hide();
            this.imgTag = res["imgTag"][0];
            this.getImages(this.imgTag);
            
        });
    }
    onFileChange(event) {
        console.log("Hello");
        this.selectedFile = event.target.files[0];
        console.log(this.selectedFile);
    }

    clearFile() {
        this.searchForm.get('avatar').setValue(null);
        this.fileInput.nativeElement.value = '';
    }

    addToWishlist(product : any){
        this.spinner.show();
        this.searchService.addToWishlist(product)
        .subscribe(res => { 
            this.spinner.hide();
            if(res.status == true){
                alert("product added successfully");
            }
            else{
                alert("Something went wrong")
            }
        },
        errmess => {
            this.spinner.hide();
            this.errMess = <any>errmess
        });
    }
}
