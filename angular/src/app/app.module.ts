import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor, UnauthorizedInterceptor } from './services/auth.interceptor';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
    MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,MatSidenavModule,
    MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatAutocompleteModule } from '@angular/material';

import { FormsModule , ReactiveFormsModule  } from '@angular/forms';
import { SearchService } from './services/search.service'
import { NgxSpinnerModule } from 'ngx-spinner';
import { AuthService } from './shared/auth/auth.service';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        MatSelectModule,
        MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
        MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule,MatNativeDateModule,
        MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule,
        MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule,MatSidenavModule,
        MatAutocompleteModule,
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, SearchService,AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UnauthorizedInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
