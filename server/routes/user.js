var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var authenticate = require('../authenticate');

var userRoutes = express.Router();
userRoutes.use(bodyParser.json());
var Product = require('./models/products');
var User = require('./models/users');


userRoutes.post('/login',function(req, res)
{    
    User.find({'phone' : req.body.phone,'password' : req.body.password}, function(err , old_user)
    {
        if(old_user.length != 0)
        {
            var token = authenticate.getToken({_id: old_user[0]._id});
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: true, token: token, status: old_user[0]["name"]});   
        }
        else
        {
            res.statusCode = 401;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: false, token:'none' , status: 'Invalid Username or Password'})
        }
    })
});

userRoutes.post('/addToWishlist' ,authenticate.verifyUser, function(req,res)
{
    Product.create(req.body ,function(err,product)
    {
        if(err)
        {
            throw err;
        }
        req.user.Cart.push(product._id); 
        req.user.save(function(err)
        {
            if(err)
            {
                throw err;
            }
            res.json({
                "success" : true
            }) 
        });     
    })
});

userRoutes.post('/addToRecommadationlist' ,authenticate.verifyUser, function(req,res)
{
    Product.create(req.body ,function(err,product)
    {
        if(err)
        {
            throw err;
        }
        req.user.product.push(product._id); 
        req.user.save(function(err)
        {
            if(err)
            {
                throw err;
            }
            res.json({
                "success" : true
            }) 
        });     
    })
});

userRoutes.get('/showWishlist' , authenticate.verifyUser, function(req , res)
{
    console.log(req.user._id);
    User.find({"_id" : req.user._id})
            .populate('Cart')
            .exec(function(err, user)
            {
                if(err)
                {
                    throw err;
                }
                else
                {
                    res.json({
                        "product" : user[0].Cart
                    });
                }
            })
})

userRoutes.get('/showRecommadationlist' , authenticate.verifyUser, function(req , res)
{
    console.log(req.user._id);
    User.find({"_id" : req.user._id})
            .populate('product')
            .exec(function(err, user)
            {
                if(err)
                {
                    throw err;
                }
                else
                {
                    res.json({
                        "product" : user[0].product
                    });
                }
            })
})

userRoutes.post('/register', function(req, res)
{    
    User.find({'phone' : req.body.phone}, function(err , old_user)
    {
        if(old_user.length != 0)
        {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: false, token: 'none', status: 'Mobile Number already Exist'});  
        }
        else
        {
            User.create(req.body , function(err,new_user)
            {
                if (err)
                {
                    throw err;
                }
                else
                {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({success: true, token: 'none', status: 'Registeration Successfully'}); 
                }
            })
        }
    })
});

userRoutes.use(passport.initialize());
module.exports = userRoutes;
